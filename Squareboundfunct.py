def bound(obst_1, obst_2):
    b = cross_test(obst_1.x, obst_1.y, CELL_SIZE, obst_2.x, obst_2.y, CELL_SIZE)
    if b:
        if obst_1.x < obst_2.x:
            print('левее 1')
            obst_1.speed_x = -(obst_1.speed_x + random.randint(1,3))
            obst_2.speed_x = -(obst_2.speed_x + random.randint(1,3))
        else:
            print('правее 1')
            obst_1.speed_x = -(obst_1.speed_x + random.randint(1,3))
            obst_2.speed_x = -(obst_2.speed_x + random.randint(1,3))
        if obst_1.y < obst_2.y:
            print('выше 1')
            obst_1.speed_y = -(obst_1.speed_y + random.randint(1,3))
            obst_2.speed_y = -(obst_2.speed_y + random.randint(1,3))
        else:
            print('ниже 1')
            obst_1.speed_y = -(obst_1.speed_y + random.randint(1,3))
            obst_2.speed_y = -(obst_2.speed_y + random.randint(1,3))           